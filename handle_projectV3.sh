#!/bin/bash

set -e

# Parse script name to get package name
package=$(echo ${0} | sed 's/.sh$//g')

DELETE=
NEW=
PRJ=

help_text="$package - Transfer or delete an ownerless project space

$package [options]

options:
-h, --help                Show brief help.
-d, --delete              Delete this project.
-n, --new-svc-acc         Specify new service account.
-p, --project             Specify project space."

# Parsing command-line arguments
while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "$help_text"
      exit 0
      ;;
    -d|--delete)
      DELETE="yes"
      shift
      ;;
    -n|--new-svc-acc)
      NEW="$2"
      shift 2
      ;;
    -p|--project)
      PRJ="$2"
      shift 2
      ;;
    *)
      break
      ;;
  esac
done

# Check for required arguments
if [ -z "$PRJ" ]; then
  echo "ERROR: Required option '-p, --project' is missing."
  echo "$help_text"
  exit 1
fi

if [ -z "$DELETE" ]; then
  if [ -z "$NEW" ]; then
    echo "ERROR: Required option '-n, --new-svc-acc' is missing."
    echo "$help_text"
    exit 1
  fi
fi

# Assigning old owner before using it
OLD=$(cernboxcop project getowner $PRJ)

# If the command fails, stop the script
if [ $? -ne 0 ]; then
  echo "Failed to get old owner of the project"
  exit 1
fi

if [ -z "$DELETE" ]; then
  echo "Starting the transfer of project: $PRJ from $OLD to $NEW ..."
else
  echo "Deleting project $PRJ ..."
fi

# Retrieve the old service account ID
OLD_SVC_ID=$(eos -r 0 0 root://eosproject-$letter.cern.ch ls -lha /eos/project/$letter/$PRJ | grep " \.$" | awk '{print $3}')

# If the command fails, stop the script
if [ $? -ne 0 ]; then
  echo "Failed to retrieve the old service account ID"
  exit 1
fi

# Transfer or delete project
if [ -z "$DELETE" ]; then
  # Retrieve the new service account ID
  NEW_SVC_ID=$(id -u "$NEW")

  # If the command fails, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to retrieve the new service account ID"
    exit 1
  fi

  # Check old sharing list
  cernboxcop sharing list | grep "$OLD"

  # If the command fails, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to check old sharing list"
    exit 1
  fi

  echo "Add new svc acc to cernbox-project-$PRJ-admins e-group"

  # Retrieve user information from the phonebook
  FULLNAME=$(phonebook "$NEW" -t displayname)
  EMAIL=$(phonebook "$NEW" -t email)
  CCID=$(phonebook "$NEW" -t ccid)

  # If the phonebook commands fail, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to retrieve user information from the phonebook"
    exit 1
  fi

  # Run the Python script to add the new service account to the e-group
  python /opt/cbox-ops/upgrade-ownerless/ownerless-egroup.py "$PRJ" "$NEW" "${FULLNAME//;}" "${EMAIL//;}" "${CCID//;}"

  # If the Python script fails, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to add the new service account to the e-group"
    exit 1
  fi

  # Check if the project should be deleted or transferred
  if [ -z "$DELETE" ]; then
    # Change ownership of the project
    eos -r 0 0 root://eosproject-$letter.cern.ch chown -r $NEW /eos/project/$letter/$PRJ
  else
    # Check if $PRJ is not empty
    if [ -z "$PRJ" ]; then
      echo "ERROR: Project name is empty. Cannot move to quarantine."
      exit 1
    fi

    # Move the project to quarantine
    eos -r 0 0 root://eosproject-$letter.cern.ch mv /eos/project/$letter/$PRJ /eos/project/.quarantine/$letter
  fi

  # If the command fails, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to process the project"
    exit 1
  fi

  if [ -z "$DELETE" ]; then
    # Get quota values
    VALUES=$(eos -r 0 0 root://eosproject-$letter.cern.ch quota ls -p /eos/project -u $OLD -m | awk -F"_" '{print $1" "$2}' | awk -F"::" '{print $NF}')
    quota_V=$(echo $VALUES | awk '{print $1}')
    quota_I=$(echo $VALUES | awk '{print $2}')

    # Set quota for the new user
    eos -r 0 0 root://eosproject-$letter.cern.ch quota set -v $quota_V -i $quota_I -p /eos/project -u $NEW
    eos -r 0 0 root://eosproject-$letter.cern.ch quota rm -p /eos/project -u $OLD
  else
    # Remove quota for the old user
    eos -r 0 0 root://eosproject-$letter.cern.ch quota rm -p /eos/project -u $OLD
  fi

  # If the command fails, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to manage quota"
    exit 1
  fi

  if [ -z "$DELETE" ]; then
    # Update the service account of the project
    cernboxcop project update-svc-account $PRJ $NEW
  else
    # Delete the project
    cernboxcop project delete $PRJ
  fi

  # If the command fails, stop the script
  if [ $? -ne 0 ]; then
    echo "Failed to manage the project"
    exit 1
  fi

  if [ -z "$DELETE" ]; then
    # Change ownership of the recycle bin
    eos -r 0 0 root://eosproject-$letter.cern.ch mv /eos/project-$letter/proc/recycle/uid:$OLD_SVC_ID/ /eos/project-$letter/proc/recycle/uid:$NEW_SVC_ID
    if [ $? -eq 0 ]; then
      echo "Changed ownership of the recycle bin to uid:$NEW_SVC_ID"
    else
      echo "Failed to change ownership of the recycle bin"
      exit 1
    fi
  else
    echo "NOT removing the recycle bin"
  fi

  if [ -z $DELETE ]; then
    # Get the backup job ID
    JOB_ID=$(ssh root@cback-backup-01 cback backup status $OLD | awk '{print $2}'| grep -P [0-9]+)

    # If the command fails, stop the script
    if [ $? -ne 0 ]; then
      echo "Failed to get the backup job ID"
      exit 1
    fi

    # Modify backup user and reset the backup
    ssh root@cback-backup-01 "cback backup modify $JOB_ID --user_name=$NEW"
    ssh root@cbox-backup-01 "cback backup reset $JOB_ID"

    # If the command fails, stop the script
    if [ $? -ne 0 ]; then
      echo "Failed to modify or reset the backup"
      exit 1
    fi
  fi
fi


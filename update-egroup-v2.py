import os
import sys
import logging
import netrc
from suds.client import Client
from suds.transport.http import HttpAuthenticated

logging.basicConfig(level=logging.INFO, filename='./suds.log')
logging.getLogger('suds.client').setLevel(logging.DEBUG)
logging.getLogger('suds.transport').setLevel(logging.DEBUG)

suds_path = os.path.join(os.environ['HOME'], 'python')
sys.path.append(suds_path)

def check_ok(reply):
    if "ErrorType" in reply:
        return False
    return True

def get_group(group_name=None):
    auth = netrc.netrc()
    login, acct, password = auth.authenticators('egroups.cern.ch')
    url = 'https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl'
    client = Client(url, username=login, password=password)
    group = client.service.FindEgroupByName(group_name).result
    return client, group

def add_service_account(e_group_name, account_name):
    client, group = get_group(e_group_name)
    logging.debug("++++++++> got template eGroup %s" % e_group_name)
    print("[E-GROUP] Going to add account %s to %s" % (account_name, e_group_name))
    try:
        new_user = group.Members
    except AttributeError:
        logging.debug("Going to add new member list ... ")
        print("[E-GROUP] Going to add new member list ... ")
    members = []
    member = client.factory.create('ns0:MemberType')
    member.Type = 'Account'
    member.Name = account_name
    members.append(member)
    overwrite_members = False
    ret = client.service.AddEgroupMembers(e_group_name, overwrite_members, members)
    if not check_ok(ret):
        print("[E-GROUP] ERROR could not add user %s to group %s" % (account_name, e_group_name))
        print("[E-GROUP] Server Response: %s" % ret)
        sys.exit(1)
    print("[E-GROUP] User %s added succesfully to %s" % (account_name, e_group_name))

if __name__ == "__main__":
    project_name = sys.argv[1].lower()
    service_account = sys.argv[2].lower()
    full_name = sys.argv[3]
    email = sys.argv[4].lower()
    ccid = sys.argv[5]
    print(email)
    group_admins = "cernbox-project-%s-admins" % project_name
    add_service_account(group_admins, service_account)
#!/bin/bash

# Configure your email settings
email_sender="cernbox@cern.ch"
email_subject="Your Project is ownerless"
storage_file="sent_reminders.txt"
log_file="OwnerlessMail_log.txt"

#check if the storage_file exist if not it creates it
[ -e "$storage_file" ] || touch "$storage_file"

# Function to check if a reminder or final email should be sent
should_send_email() {
    local email=$1
    local current_output=$2
    local timestamp=$(date +%s)
    local entry=$(grep -F "$email" "$storage_file")
    local last_sent=$(echo "$entry" | cut -d '|' -f2)
    local email_type=$(echo "$entry" | cut -d '|' -f3)

    if [ -z "$last_sent" ]; then
        echo "$email|$timestamp|first" >> "$storage_file"
        return 0
    fi

    local diff=$((timestamp - last_sent))
    local days_diff=$((diff / 86400))

    if [ "$days_diff" -ge 7 ] && [ "$email_type" = "first" ] && echo "$current_output" | grep -q "$email"; then
        sed -i "s/$email|$last_sent|$email_type/$email|$timestamp|reminder/" "$storage_file"
        return 1
    elif [ "$days_diff" -ge 10 ] && [ "$email_type" = "reminder" ] && echo "$current_output" | grep -q "$email"; then
        sed -i "s/$email|$last_sent|$email_type/$email|$timestamp|final/" "$storage_file"
        return 2
    fi

    return 3
}

generate_first_email_body() {
    local project_name=$1
    cat << EOF
Dear users of the CERNBox project space "${project_name}",

During routine checks, we have detected that the service account owning your project space has expired and presumably it is not in use any longer. Therefore, the project space is expected to be archived.

ACTION: if you are still actively using this project space, please create a new service account and contact us at [https://cern.service-now.com/service-portal?id=service_element&name=CERNBox-Service] to communicate it, such that the project can be reassigned. If no action is taken, the project will be archived in 30 days from now.

Best regards,
The CERNBox service
EOF
}

generate_reminder_email_body() {
    local project_name=$1
    cat << EOF
Dear users of the CERNBox project space "${project_name}",

This is a reminder concerning your project space, which will soon be archived.

ACTION: if you are still actively using this project space, please create a new service account and contact us at [https://cern.service-now.com/service-portal?id=service_element&name=CERNBox-Service] to communicate it, such that the project can be reassigned. If no action is taken, the project will be archived in 7 days from now.

Best regards,
The CERNBox service
EOF
}

generate_final_email_body() {
    local project_name=$1
    cat << EOF
Dear users of the CERNBox project space "${project_name}",

This is to inform you that the project space has been archived, and the corresponding e-groups deleted.

Best regards,
The CERNBox service
EOF
}

# Run the cernboxcop project ownerless command and store the output in a variable
output=$(cernboxcop project ownerless)
timestamp=$(date "+%Y-%m-%d %H:%M:%S")

# Log the cernboxcop command execution
echo "[$timestamp] Checked cernboxcop project ownerless command output." >> "$log_file"

# Iterate over the output and send emails
cernboxcop project ownerless | while read -r line; do
    email_recipient="cernbox-project-${line}-admins@cern.ch"
    
    email_status=$(should_send_email "$email_recipient" "$output")
    
    timestamp=$(date "+%Y-%m-%d %H:%M:%S")
    
    if [ ! -z "$email_status" ] && [ "$email_status" -eq 0 ]; then
        # Send the first email using sendmail
        printf "Subject: %s\n\n%s" "$email_subject" "$(generate_first_email_body "$line")" | sendmail -f "$email_sender" "$email_recipient"
        echo "[$timestamp] Sent first email to $email_recipient." >> "$log_file"
    elif [ ! -z "$email_status" ] && [ "$email_status" -eq 1 ]; then
        # Send the reminder email using sendmail
        printf "Subject: %s\n\n%s" "$email_subject" "$(generate_reminder_email_body "$line")" | sendmail -f "$email_sender" "$email_recipient"
        echo "[$timestamp] Sent reminder email to $email_recipient." >> "$log_file"
    elif [ ! -z "$email_status" ] && [ "$email_status" -eq 2 ]; then
        # Send the final email using sendmail
        printf "Subject: %s\n\n%s" "$email_subject" "$(generate_final_email_body "$line")" | sendmail -f "$email_sender" "$email_recipient"
        echo "[$timestamp] Sent final email to $email_recipient." >> "$log_file"
    fi
done <<< "$output"

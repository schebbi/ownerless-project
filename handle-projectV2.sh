#!/bin/bash

set -e

package=$(echo ${0} | sed 's/.sh$//g')

DELETE=
NEW=
PRJ=

help_text="$package - Transfer or delete an ownerless project space

$package [options]

options:
-h, --help                Show brief help.
-d, --delete              Delete this project.
-n, --new-svc-acc         Specify new service account.
-p, --project             Specify project space."

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "$help_text"
      exit 0
      ;;
    -d|--delete)
      DELETE="yes"
      shift 1
      ;;
    -n|--new-svc-acc)
      NEW="$2"
      shift 2
      ;;
    -p|--project)
      PRJ="$2"
      shift 2
      ;;
    *)
      break
      ;;
  esac
done

if [ -z "$PRJ" ]; then
  echo "ERROR: Required option '-p, --project' is missing."
  echo "$help_text"
  exit 1
fi

if [ -z "$DELETE" ]; then
  if [ -z "$NEW" ]; then
    echo "ERROR: Required option '-n, --new-svc-acc' is missing."
    echo "$help_text"
    exit 1
  fi
  echo "Starting the transfer of project: $PRJ from $OLD to $NEW ..."
else
  echo "Deleting project $PRJ ..."
fi

OLD=$(cernboxcop project getowner $PRJ)
letter=${PRJ:0:1}
OLD_SVC_ID=$(eos -r 0 0 root://eosproject-$letter.cern.ch ls -lha /eos/project/$letter/$PRJ | grep " \.$" | awk '{print $3}')

if [ -z "$DELETE" ]; then
  NEW_SVC_ID=$(id -u "$NEW")

  cernboxcop sharing list | grep "$OLD"

  echo "Add new svc acc to cernbox-project-$PRJ-admins e-group"
  FULLNAME=$(phonebook "$NEW" -t displayname)
  EMAIL=$(phonebook "$NEW" -t email)
  CCID=$(phonebook "$NEW" -t ccid)
  python /opt/cbox-ops/upgrade-ownerless/ownerless-egroup.py "$PRJ" "$NEW" "${FULLNAME//;}" "${EMAIL//;}" "${CCID//;}"

if [ -z "$DELETE" ]; then
  eos -r 0 0 root://eosproject-$letter.cern.ch chown -r $NEW /eos/project/$letter/$PRJ
else
  eos -r 0 0 root://eosproject-$letter.cern.ch mv /eos/project/$letter/$PRJ /eos/project/.quarantine/$letter
fi

if [ -z "$DELETE" ]; then
  VALUES=$(eos -r 0 0 root://eosproject-$letter.cern.ch quota ls -p /eos/project -u $OLD -m | awk -F"_" '{print $1" "$2}' | awk -F"::" '{print $NF}')
  quota_V=$(echo $VALUES | awk '{print $1}')
  quota_I=$(echo $VALUES | awk '{print $2}')

  eos -r 0 0 root://eosproject-$letter.cern.ch quota set -v $quota_V -i $quota_I -p /eos/project -u $NEW
  eos -r 0 0 root://eosproject-$letter.cern.ch quota rm -p /eos/project -u $OLD
else
  eos -r 0 0 root://eosproject-$letter.cern.ch quota rm -p /eos/project -u $OLD
fi

if [ -z "$DELETE" ]; then
  cernboxcop project update-svc-account $PRJ $NEW
else
  cernboxcop project delete $PRJ
fi

if [ -z "$DELETE" ]; then
  eos -r 0 0 root://eosproject-$letter.cern.ch mv /eos/project-$letter/proc/recycle/uid:<old_uid>/ /eos/project-$letter/proc/recycle/uid:<new_uid>
  if [ $? -eq 0 ]; then
    echo "Changed ownership of the recycle bin to uid:<new_uid>"
  else
    echo "Failed to change ownership of the recycle bin"
  fi
else
  echo "NOT removing the recycle bin"
fi

if [ -z $DELETE ]; then
  JOB_ID=$(ssh root@cback-backup-01 cback backup status $OLD | awk '{print $2}'| grep -P [0-9]+)
  ssh root@cback-backup-01 "cback backup modify $JOB_ID --user_name=$NEW"
  ssh root@cbox-backup-01 "cback backup reset $JOB_ID"
fi
